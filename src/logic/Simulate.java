package logic;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import management.ArithmeticFunctionalUnit;
import management.BufferEntry;
import management.CommonUtility;
import management.MachineState;
import management.OpCodes;
import management.ReorderBufferEntry;
import management.ReservationStationEntry;

public class Simulate {
	
	public static void main(String args[]) throws FileNotFoundException, CloneNotSupportedException{
		
		MachineState currentState = new MachineState();
		MachineState newState;
		//--------------- local variables ---------------//
		int i; //generic counter variable for internal loop constructs
		String line = null; //temp storage for holding file contents while filling machine memory
		
		//--------------- initial setup ----------------//
		//read in the contents of the file to run
		try {
			//set up buffered reader for reading
			BufferedReader reader = new BufferedReader(new FileReader(args[0]));
			
			//iterate through program and load all instructions into instruction memory
			i = 0;
			while ((line = reader.readLine()) != null) {
				currentState.instructionMemory[i] = Integer.parseInt(line);
				System.out.println("This is from the file: " + CommonUtility.getInstructionString(currentState.instructionMemory[i]));
				i++;
				currentState.instructionMemoryTail++;
			}
			System.out.println("Instruction Memory Tail: " + currentState.instructionMemoryTail);
			
			//dispose of the file reader once we are done with file reading.
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		//now we have the contents of the file in instruction memory. Synchronize instruction and data memory.
		for(i = 0; i < currentState.instructionMemory.length; i++){
			currentState.dataMemory[i] = currentState.instructionMemory[i];
		}
		

		while (true){
			currentState.printState();

			newState = new MachineState(currentState);
			//update state
			newState = fetch(currentState, newState);
			
			newState = rename(currentState, newState);
			
			newState = allocate(currentState, newState);
			
			newState = schedule(currentState, newState);
			
			newState = execute(currentState, newState);
			
			newState = commit(currentState, newState);
			
			currentState = new MachineState(newState);

		}

	}
	
	/***
	 * fetch() performs the fetch stage of the pipeline. In this stage we do the following:
	 * 1) Pull two instructions into the fetchBuffer
	 * 2) Check instructions for the following conditions
	 * 	a) if both instructions are branches, only keep the first instruction in program order
	 * 	b) if the first instruction is a branch that is predicted to be taken, keep only that instruction
	 * 	c) else keep both instructions in the fetch buffer to be read by rename
	 * @throws CloneNotSupportedException 
	 */
	public static MachineState fetch(MachineState currentState, MachineState nextState){
		//if there is a structural hazard in the currentState, don't do anything.
		if(currentState.robHazard){
			return nextState;
		}
		
		//pull instruction1 into the buffer
		nextState.fetchBuffer[0].instruction = currentState.instructionMemory[currentState.pc];

		//set appropriate pc
		nextState.fetchBuffer[0].pcPlusOne = currentState.pc + 1;

		//pull instruction2 into the buffer
		nextState.fetchBuffer[1].instruction = currentState.instructionMemory[currentState.pc+1];

		//set appropriate pc
		nextState.fetchBuffer[1].pcPlusOne = currentState.pc + 2;
		
		//check to see if we should send both or only one instruction forward...
		//If the first instruction is a branch... let's figure out if we need to take that branch
		if(CommonUtility.getOpCode(nextState.fetchBuffer[0].instruction) == OpCodes.BEQ.ordinal()){
			nextState.branchPredictor.tryInstall(currentState.pc, nextState.fetchBuffer[0].instruction);
			nextState.fetchBuffer[0].branchPredictedTaken = nextState.branchPredictor.predict(nextState.fetchBuffer[0].instruction);
		} 
		
		//if the first instruction is a branch and we predicted to take it....
		if(CommonUtility.getOpCode(nextState.fetchBuffer[0].instruction) == OpCodes.BEQ.ordinal() && nextState.fetchBuffer[0].branchPredictedTaken){
			//first instruction is valid
			nextState.fetchBuffer[0].valid = true;
			
			//second instruction is not valid
			nextState.fetchBuffer[1].valid = false;
			
			//only sending one instruction forward
			nextState.fetchPcIncrement = 1;
			nextState.fetched +=1;
			
			//update the PC to be the target
			nextState.pc = nextState.branchPredictor.getTarget(nextState.fetchBuffer[0].instruction) - nextState.fetchPcIncrement;
			
			//destroy the buffers because they have irrelevant stuff in them now
			for(int i = 0; i < nextState.allocateBuffer.length; i++){
				nextState.allocateBuffer[i] = new BufferEntry();
			}
			for(int i = 0; i < nextState.renameBuffer.length; i++){
				nextState.renameBuffer[i] = new BufferEntry();
			}
			
		}else if(CommonUtility.getOpCode(currentState.fetchBuffer[0].instruction) == OpCodes.BEQ.ordinal() &&
					CommonUtility.getOpCode(currentState.fetchBuffer[1].instruction) == OpCodes.BEQ.ordinal()){
			//if both instructions are branches, don't worry about the second instruction
			//first instruction is valid
			nextState.fetchBuffer[0].valid = true;
			
			//second instruction is not valid
			nextState.fetchBuffer[1].valid = false;
			
			//only sending one instruction forward
			nextState.fetchPcIncrement = 1;
			nextState.fetched +=1;
		} else{
			
			//if neither of the corner cases are hit...
			//check instruction2 to see if it is a branch
			if(CommonUtility.getOpCode(nextState.fetchBuffer[1].instruction) == OpCodes.BEQ.ordinal()){
				nextState.branchPredictor.tryInstall(currentState.pc + 1, nextState.fetchBuffer[1].instruction);
				nextState.fetchBuffer[1].branchPredictedTaken = nextState.branchPredictor.predict(nextState.fetchBuffer[1].instruction);
			}
			
			//first instruction is valid
			nextState.fetchBuffer[0].valid = true;
			
			//second instruction is valid
			nextState.fetchBuffer[1].valid = true;
			
			//sending two instructions forward
			nextState.fetchPcIncrement = 2;
			nextState.fetched += 2;
			
			//if the second instruction is a predicted branch, we need to update the pc
			if(CommonUtility.getOpCode(nextState.fetchBuffer[1].instruction) == OpCodes.BEQ.ordinal() && nextState.fetchBuffer[1].branchPredictedTaken){
				nextState.pc = nextState.branchPredictor.getTarget(nextState.fetchBuffer[1].instruction) - nextState.fetchPcIncrement;
				
				//destroy the buffers because they have irrelevant stuff in them now
				for(int i = 0; i < nextState.renameBuffer.length; i++){
					nextState.renameBuffer[i] = new BufferEntry();
				}
			}
		}
		
		//go through the buffer and clean up fringe cases
		for(int i = 0; i < nextState.fetchBuffer.length; i++){
			//make sure no weird data instructions inadvertently modify the renameTable
			if(CommonUtility.getOpCode(nextState.fetchBuffer[i].instruction) < 0 || CommonUtility.getOpCode(nextState.fetchBuffer[i].instruction) > 7){
				nextState.fetchBuffer[i].valid = false;
				nextState.fetchPcIncrement--;
			}
		}
		if((currentState.pc >= currentState.instructionMemoryTail)){
			nextState.fetchBuffer[0].valid = false;
			nextState.fetchPcIncrement--;
		}
		if(currentState.pc >= currentState.instructionMemoryTail){
			nextState.fetchBuffer[1].valid = false;
			nextState.fetchPcIncrement--;
		}
		
		return nextState;
	}

	/***
	 * rename() performs the rename stage of the pipeline. In this stage we do the following:
	 * 1) Pull over two instructions from the fetchBuffer into the renameBuffer
	 * 2) On each of those instructions
	 * 	a) check the re-order buffer to see if any instructions are writing to the destination register of this instruction
	 * 		i) if so, place an appropriate rename table entry into the rename table linking the rob location with this physical dest reg
	 * 		ii) if not, proceed to the next instruction
	 */
	public static MachineState rename(MachineState currentState, MachineState nextState){
		//if there is a structural hazard in the currentState, don't do anything.
		if(currentState.robHazard){
			return nextState;
		}

		//pull from the fetchBuffer into the renameBuffer
		for(int i = 0; i < currentState.fetchBuffer.length; i++){
			nextState.renameBuffer[i] = currentState.fetchBuffer[i];
		}
		
		//now iterate through the rename buffer and process things
		for(int i = 0; i < nextState.renameBuffer.length; i++){
			
			//if the instruction is valid
			if(nextState.renameBuffer[i].valid){
				//rename all appropriate source registers in the buffer entry
				nextState.renameBuffer[i] = currentState.renameTable.rename(nextState.renameBuffer[i]);

			}	
		}
		
		return nextState;
	}
	
	/*** allocate() performs the allocate stage of the pipeline. In this stage we do the following:
	 * 1) Pull instructions from the rename buffer into the allocare buffer
	 * 2) Put those instructions into the reorder buffer
	 * 3) Check each reservation station bank and ensure that each bank remains as saturated as possible each cycle
	 * @param currentState
	 * @param nextState
	 * @return
	 */
	public static MachineState allocate(MachineState currentState, MachineState nextState){
		//if there is a structural hazard in the currentState, don't do anything.
		if(currentState.robHazard){
			//check to see if there is still a hazard
			if(nextState.reorderBuffer.entries[nextState.robHead].inUse == true){
				return nextState;
			} else {
				nextState.robHazard = false;
			}
		}
		
		//local variables
		int destLoc;
		int robIndexToInstall;
		
		int tempInstruction;
		int tempOpCode;
		
		//pull instructions over into allocateBuffer
		//pull from the fetchBuffer into the renameBuffer
		for(int i = 0; i < currentState.renameBuffer.length; i++){
			nextState.allocateBuffer[i] = currentState.renameBuffer[i];
		}
		
		//allocate an entry at the head of the rob
		//check for a structural hazard
		//TODO: this is more restrictive than it could be. Here we check to see if we have a full two instruction gap in the rob, if we don't then we consider that a structural hazard.
		//so, we really report a structural hazard one commit too soon, as we should be able to put one instruction into the rob and then just wait. We are totally out of time though.
		if(nextState.reorderBuffer.entries[nextState.robHead].inUse == true || nextState.reorderBuffer.entries[nextState.getNextRobHead()].inUse == true){
			//the next rob entry to allocate is already in use, time to throw a hazard flag and wait out the storm
			System.out.println("PC: " + currentState.pc);
			nextState.robHazard = true;
			return nextState;
			}

		
		//for each instruction in the allocate buffer
		for(int i = 0; i < nextState.allocateBuffer.length; i++){
			//store temp variables for this iteration
			tempInstruction = nextState.allocateBuffer[i].instruction;
			tempOpCode = CommonUtility.getOpCode(tempInstruction);
			
			//if the instruction is valid
			if(nextState.allocateBuffer[i].valid){

				//reallocate the specified buffer entry on the heap
				nextState.reorderBuffer.entries[nextState.robHead] = new ReorderBufferEntry();
				nextState.reorderBuffer.entries[nextState.robHead].inUse = true;
				
				//calculate the destReg 
				if(tempOpCode == OpCodes.BEQ.ordinal() || tempOpCode == OpCodes.SW.ordinal()){
					destLoc = -1; // nothing to write to
				} else if(tempOpCode == OpCodes.LW.ordinal()){
					destLoc = CommonUtility.getField1(tempInstruction); //writes to regB
				} else {
					destLoc = CommonUtility.getField2(tempInstruction); //writes to destReg
				}
				
				//if we have a destReg
				if(destLoc > 0){
					//update the rename table
					nextState.renameTable.entries[destLoc].renameIndex = nextState.robHead;
					nextState.renameTable.entries[destLoc].valid = true;
					
					//Now we have a problem... if instructions using this register were fetched between the beginning of the pipeline and now, we also need to rename those instructions...
					//if this is the first item in the allocate buffer, check the second entry
					if(i == 0 && nextState.allocateBuffer[1].valid){
						nextState.allocateBuffer[1] = nextState.renameTable.rename(nextState.allocateBuffer[1]);
					}
					//now we need to make sure the stuff that we just renamed in the previous step of the pipeline is not stale...
					for(int j = 0; j < nextState.renameBuffer.length; j++){
						nextState.renameBuffer[j] = nextState.renameTable.rename(nextState.renameBuffer[j]);
					}
					//Note that the fetch buffer doesn't matter, because by the time those instructions get to the rename stage, this change will be reflected in the rename table.
					
				}
				
				//set the pc value
				nextState.reorderBuffer.entries[nextState.robHead].pcPlusOne = nextState.allocateBuffer[i].pcPlusOne;				
				
				//set the destination
				nextState.reorderBuffer.entries[nextState.robHead].destRegisterId = destLoc;
				
				//set the renamed reg values
				nextState.reorderBuffer.entries[nextState.robHead].sourceReg1 = nextState.allocateBuffer[i].sourceReg1;
				nextState.reorderBuffer.entries[nextState.robHead].sourceReg2 = nextState.allocateBuffer[i].sourceReg2;
				
				//this instruction hasn't been scheduled, so the valid bit is false
				//Note: default constructor sets valid to false, setting explicitly here for clarity
				//Note: valid bit is false, so the value is irrelevant.
				nextState.reorderBuffer.entries[nextState.robHead].destRegisterValueValid = false;
				
				//if this is a noop instruction or a halt instruction, we don't have to wait on anything to happen.
				if(tempOpCode == OpCodes.NOOP.ordinal() || tempOpCode == OpCodes.HALT.ordinal()){
					nextState.reorderBuffer.entries[nextState.robHead].destRegisterValueValid = true;
				}
				
				//pass the branch prediction into the rob entry
				nextState.reorderBuffer.entries[nextState.robHead].branchPredictedTaken = nextState.allocateBuffer[i].branchPredictedTaken;
				
				//drop the instruction into the rob so that when the RS/FU are ready, it can be scheduled
				nextState.reorderBuffer.entries[nextState.robHead].instruction = tempInstruction;
				
				//increment the robHead
				nextState.robHead = nextState.getNextRobHead();
			}
			
		}
		
		//check the addRS to see if we can put any instructions into it
		//check each addRS entry
		for(int i = 0; i < currentState.addRS.length; i++){
			
			//if the entry has a noop as an operation, that means the RS is not in use
			if(currentState.addRS[i].opcode == OpCodes.NOOP.ordinal()){
				
				//search for eligible rob entry
				robIndexToInstall = nextState.reorderBuffer.getReorderBufferEntryToInstall(OpCodes.ADD, nextState.robHead);
				
				//if the robIndexToInstall comes back as -1, then there is nothing waiting in the rob to be scheduled, break out of this loop and continue
				if(robIndexToInstall < 0){
					break;
				}
				
				//make sure we are working with a clean memory slot so there is no reference confusion
				nextState.addRS[i] = new ReservationStationEntry();
				
				//This is for debug/display only, we don't actually use this in processing
				nextState.addRS[i].instruction = nextState.reorderBuffer.entries[robIndexToInstall].instruction;
				
				//set the pc value. Note that only the beq instruction really cares about the pc, so the other FUs just ignore this value, as they do with 
				//	the branch prediction flags...
				nextState.addRS[i].pcPlusOne = nextState.reorderBuffer.entries[robIndexToInstall].pcPlusOne;
				
				//set the rob as installed for the current state
				nextState.reorderBuffer.entries[robIndexToInstall].installedToRs = true;
				
				//persist state because the entry will remain scheduled as long as it is not committed.
				nextState.reorderBuffer.entries[robIndexToInstall].installedToRs = true;
				
				//fill up the fields
				//first, get the phys reg mappings
				nextState.addRS[i].source1Reg = nextState.reorderBuffer.entries[robIndexToInstall].sourceReg1;
				nextState.addRS[i].source2Reg = nextState.reorderBuffer.entries[robIndexToInstall].sourceReg2;
				
				//set the operation
				nextState.addRS[i].opcode = CommonUtility.getOpCode(nextState.reorderBuffer.entries[robIndexToInstall].instruction);
				
				//set destReg
				nextState.addRS[i].destReg = robIndexToInstall;
				
				//if this is a branch, we should pass over the offset
				if(nextState.addRS[i].opcode == OpCodes.BEQ.ordinal()){
					nextState.addRS[i].branchOffset = CommonUtility.getImmediate(nextState.reorderBuffer.entries[robIndexToInstall].instruction);
				}
				
				//now check the rename table to figure out if the source registers are valid or not
				//check source1
				//if source1 register is -1, then we are looking at a register that is not being modified by anything, so read it from reg file
				if(nextState.addRS[i].source1Reg < 0 ){
					nextState.addRS[i].source1Value = nextState.reg[CommonUtility.getField0(nextState.reorderBuffer.entries[robIndexToInstall].instruction)];
					
					//after we have fetched the value from the register file, we know the value is valid
					nextState.addRS[i].source1Valid = true;
				} else{
					//otherwise, we are waiting on something in the ROB to finish updating that register, so just wait
					nextState.addRS[i].source1Valid = false; //reset this to false because I'm paranoid
				}
				
				//check source2
				//if the source link is negative, then the source val can be read direct from the reg file
				if(nextState.addRS[i].source2Reg < 0){
					nextState.addRS[i].source2Value = nextState.reg[CommonUtility.getField1(nextState.reorderBuffer.entries[robIndexToInstall].instruction)];

					//after we have fetched the value from the register file, we know the value is valid
					nextState.addRS[i].source2Valid = true;
				} else{
					//otherwise, we are waiting on something in the ROB to finish updating that register, so just wait
					nextState.addRS[i].source2Valid = false;
				}
			}
		} // end addRS allocations
		
		//now we need to check the multRS and do the same thing
		//check each multRS entry
		for(int i = 0; i < currentState.multRS.length; i++){
			
			//if the entry has a noop as an operation, that means the RS is not in use
			if(currentState.multRS[i].opcode == OpCodes.NOOP.ordinal()){
				//search for eligible rob entry
				robIndexToInstall = nextState.reorderBuffer.getReorderBufferEntryToInstall(OpCodes.MULT, nextState.robHead);
				//if the robIndexToInstall comes back as -1, then there is nothing waiting in the rob to be scheduled, break out of this loop and continue
				if(robIndexToInstall < 0){
					break;
				}
				
				//make sure we are working with a clean memory slot so there is no reference confusion
				nextState.multRS[i] = new ReservationStationEntry();
				
				//This is for debug/display only, we don't actually use this in processing
				nextState.multRS[i].instruction = nextState.reorderBuffer.entries[robIndexToInstall].instruction;

				//set the rob as installed for the current state
				nextState.reorderBuffer.entries[robIndexToInstall].installedToRs = true;
				
				//persist state because the entry will remain scheduled as long as it is not committed.
				nextState.reorderBuffer.entries[robIndexToInstall].installedToRs = true;
				
				//fill up the fields
				//first, get the phys reg mappings
				nextState.multRS[i].source1Reg = nextState.reorderBuffer.entries[robIndexToInstall].sourceReg1;
				nextState.multRS[i].source2Reg = nextState.reorderBuffer.entries[robIndexToInstall].sourceReg2;
				
				//set the operation
				nextState.multRS[i].opcode = CommonUtility.getOpCode(nextState.reorderBuffer.entries[robIndexToInstall].instruction);
				
				//set the dest reg.
				nextState.multRS[i].destReg = robIndexToInstall;
				
				//now check the rename table to figure out if the source registers are valid or not
				//check source1
				//if source1 register is -1, then we are looking at a register that is not being modified by anything, so read it from reg file
				if(nextState.multRS[i].source1Reg < 0 ){
					nextState.multRS[i].source1Value = nextState.reg[CommonUtility.getField0(nextState.reorderBuffer.entries[robIndexToInstall].instruction)];
					//after we have fetched the value from the register file, we know the value is valid
					nextState.multRS[i].source1Valid = true;
				} else{
					//otherwise, we are waiting on something in the ROB to finish updating that register, so just wait
					nextState.multRS[i].source1Valid = false; //reset this to false because I'm paranoid
				}
				
				//check source2
				//if source2 register is -1, then we are looking at a register that is not being modified by anything, so read it from reg file
				if(nextState.multRS[i].source2Reg < 0){
					nextState.multRS[i].source2Value = nextState.reg[CommonUtility.getField1(nextState.reorderBuffer.entries[robIndexToInstall].instruction)];
					//after we have fetched the value from the register file, we know the value is valid
					nextState.multRS[i].source2Valid = true;
				} else{
					//otherwise, we are waiting on something in the ROB to finish updating that register, so just wait
					nextState.multRS[i].source2Valid = false;
				}
			}
		} //end multRS allocations
		
		//now we allocate the memRS
		for(int i = 0; i < currentState.memRS.length; i++){
			
			//if the entry has a noop as an operation, that means the RS is not in use
			if(currentState.memRS[i].opcode == OpCodes.NOOP.ordinal()){
				
				//search for eligible rob entry
				robIndexToInstall = nextState.reorderBuffer.getReorderBufferEntryToInstall(OpCodes.LW, nextState.robHead);
				
				//if the robIndexToInstall comes back as -1, then there is nothing waiting in the rob to be scheduled, break out of this loop and continue
				if(robIndexToInstall < 0){
					break;
				}
				
				//make sure we are working with a clean memory slot so there is no reference confusion
				nextState.memRS[i] = new ReservationStationEntry();
				
				//for display/debug only
				nextState.memRS[i].instruction = nextState.reorderBuffer.entries[robIndexToInstall].instruction;
				
				//set the rob as installed for the current state
				nextState.reorderBuffer.entries[robIndexToInstall].installedToRs = true;
				
				//persist state because the entry will remain scheduled as long as it is not committed.
				nextState.reorderBuffer.entries[robIndexToInstall].installedToRs = true;
				
				//fill up the fields
				//first, get the phys reg mappings
				nextState.memRS[i].source1Reg = nextState.reorderBuffer.entries[robIndexToInstall].sourceReg1;
				nextState.memRS[i].source2Reg = nextState.reorderBuffer.entries[robIndexToInstall].sourceReg2;

				
				//set the operation
				nextState.memRS[i].opcode = CommonUtility.getOpCode(nextState.reorderBuffer.entries[robIndexToInstall].instruction);
				
				//	I posit we cannot because multiple robs could be writing to the same dest reg.
				nextState.memRS[i].destReg = robIndexToInstall;
				
				//now check the rename table to figure out if the source registers are valid or not
				//check source1
				//if source1 register is -1, then we are looking at a register that is not being modified by anything, so read it from reg file
				if(nextState.memRS[i].source1Reg < 0 ){
					nextState.memRS[i].source1Value = nextState.reg[CommonUtility.getField0(nextState.reorderBuffer.entries[robIndexToInstall].instruction)];
					//after we have fetched the value from the register file, we know the value is valid
					nextState.memRS[i].source1Valid = true;
				} else{
					//otherwise, we are waiting on something in the ROB to finish updating that register, so just wait
					nextState.memRS[i].source1Valid = false; //reset this to false because I'm paranoid
				}
				
				//if the instruction is a store...
				if(nextState.memRS[i].opcode == OpCodes.SW.ordinal()){
					
					//check source2
					//if source2 register is -1, then we are looking at a register that is not being modified by anything, so read it from reg file
					if(nextState.memRS[i].source2Reg < 0 ){
						nextState.memRS[i].source2Value = nextState.reg[CommonUtility.getField1(nextState.reorderBuffer.entries[robIndexToInstall].instruction)];
		
						//after we have fetched the value from the register file, we know the value is valid
						nextState.memRS[i].source2Valid = true;
					} else{
						//otherwise, we are waiting on something in the ROB to finish updating that register, so just wait
						nextState.memRS[i].source2Valid = false; //reset this to false because I'm paranoid
					}
					
					//reuse the branchOffset to send the immediate for sw... sort of a misnomer, but I don't want to break something before submission
					nextState.memRS[i].branchOffset = CommonUtility.getImmediate(nextState.reorderBuffer.entries[robIndexToInstall].instruction);
				}else{
					//check source2
					// for lw, source2 is just an immediate, so we just set it and set valid. This is just easier for our implementation of lw... kind of nonsensical looking back on it.
					nextState.memRS[i].source2Value = CommonUtility.getImmediate(nextState.reorderBuffer.entries[robIndexToInstall].instruction);
					nextState.memRS[i].source2Valid = true;
				}
				

			}
		} //end memRS allocations
		
		return nextState;
	}

	/***schedule() performs the schedule stage of the pipeline. In this stage we do the following:
	 * 1) Check each Functional Unit to see if it is available for processing
	 * 	a) if the functional unit is available, schedule the oldest available instruction onto the FU
	 * @param currentState
	 * @param nextState
	 * @return
	 */
	public static MachineState schedule(MachineState currentState, MachineState nextState){
		//local variables
		int schedulerIndex; //storage to test for valid RS index to schedule
		//check the addFU to see if it is available
		if(currentState.addFU.available){
			//check the addRS for instructions that can be sent to the FU
			schedulerIndex = getRSEntryToSchedule(currentState.addRS, currentState.robHead);
			
			//if the scheduler index is >= 0 and < addRS.length time to schedule that dude
			if(schedulerIndex >=0 && schedulerIndex < currentState.addRS.length){
				//fill all of the FU properties from the selected RS
				nextState.addFU.available = false;
				nextState.addFU.sourceValue1 = currentState.addRS[schedulerIndex].source1Value;
				nextState.addFU.sourceValue2 = currentState.addRS[schedulerIndex].source2Value;
				nextState.addFU.robIndex = currentState.addRS[schedulerIndex].destReg;
				nextState.addFU.opcode = currentState.addRS[schedulerIndex].opcode;
				nextState.addFU.pcPlusOne = currentState.addRS[schedulerIndex].pcPlusOne;
				//NOTE: This is probably pretty confusing. When the addFU kicks on in execute, it expects the offset to initially be in the target slot.
				//	after comparison, it will appropriately update the target by adding pc + 1 or ignoring the existence of the value entirely.
				nextState.addFU.branchTarget = currentState.addRS[schedulerIndex].branchOffset;
				
				//free up the RS we just put onto the FU
				nextState.addRS[schedulerIndex].deallocate();
			} else if (schedulerIndex > currentState.addRS.length){
				//uh... what? This probably means there is a bug in the selection method that we didn't find... alert user, application will likely not function as expected henceforth...
				System.out.println("Fatal Error: scheduler index in schedule routine returned a state larger than the number of reservation stations allocated to the machine...");				
			}
		}
		
		//check the multRS for instructions that can be sent to the FU
		//Note: this FU is pipelined. 
		//	In this stage of the CPU pipeline, we should only be looking at the "low" stage of the mult pipeline
		if(currentState.multFU[0].available){
			//check the multRS for instructions that can be sent to the FU
			schedulerIndex = getRSEntryToSchedule(currentState.multRS, currentState.robHead);
			
			//if the scheduler index is >= 0 and < memRS.length time to schedule that dude
			if(schedulerIndex >=0 && schedulerIndex < currentState.multRS.length){
				
				//fill all of the FU properties from the selected RS
				nextState.multFU[0].available = false;
				nextState.multFU[0].sourceValue1 = currentState.multRS[schedulerIndex].source1Value;
				nextState.multFU[0].sourceValue2 = currentState.multRS[schedulerIndex].source2Value;
				nextState.multFU[0].robIndex = currentState.multRS[schedulerIndex].destReg;
				nextState.multFU[0].opcode = currentState.multRS[schedulerIndex].opcode;
				
				//free up the RS we just put onto the FU
				nextState.multRS[schedulerIndex].deallocate();
			} else if (schedulerIndex > currentState.multRS.length){
				//uh... what? This probably means there is a bug in the selection method that we didn't find... alert user, application will likely not function as expected henceforth...
				System.out.println("Fatal Error: scheduler index in schedule routine returned a state larger than the number of reservation stations allocated to the machine...");				
			}

		}
		
		//check the memRS for instructions that can be sent to the FU
		if(currentState.memFU.available){
			//check the memRS for instructions that can be sent to the FU
			schedulerIndex = getRSEntryToSchedule(currentState.memRS, currentState.robHead);
			
			//if the scheduler index is >=0 and < memRS.length, time to schedule that dude
			if(schedulerIndex >= 0 && schedulerIndex < currentState.memRS.length){
				
				//fill all of the FU properties from the selected RS
				nextState.memFU.available = false;
				nextState.memFU.sourceValue1 = currentState.memRS[schedulerIndex].source1Value;
				nextState.memFU.sourceValue2 = currentState.memRS[schedulerIndex].source2Value;
				nextState.memFU.robIndex = currentState.memRS[schedulerIndex].destReg;
				nextState.memFU.opcode = currentState.memRS[schedulerIndex].opcode;
				nextState.memFU.branchTarget = currentState.memRS[schedulerIndex].branchOffset;
				
				//free up the RS we just put onto the FU
				nextState.memRS[schedulerIndex].deallocate();
			} else if (schedulerIndex > currentState.memRS.length){
				//something terrible happened...
				System.out.println("Fatal Error: scheduler index in schedule routine returned a state larger than the number of reservation stations allocated to the machine...");				
			}
		}
		
		return nextState;
	}
	
	/***
	 * execute() performs the execute stage of the pipeline. In this stage we do the following:
	 * 1) Check each FU and process the outcome.
	 * 2) Update the ROB
	 * 3) Broadcast the result
	 * @param rsBank
	 * @param robHead
	 * @return
	 */
	public static MachineState execute(MachineState currentState, MachineState nextState){
		//if the add FU is ready, execute
		if(!currentState.addFU.outcomeReady && !currentState.addFU.available){
			switch(currentState.addFU.opcode){
				case 0: //this is an add
					nextState.addFU.cycleCounter = 1;
					nextState.addFU = currentState.addFU.performAdd(nextState.addFU);
					break;
				case 1: //this is a nand
					nextState.addFU.cycleCounter = 1;
					nextState.addFU = currentState.addFU.performNand(nextState.addFU);
					break;
				case 4: //this is a beq
					nextState.addFU.cycleCounter = 1;
					nextState = currentState.addFU.performBeq(nextState);
					break;
				default: 
					System.out.println("Unexpected opcode in the add FU... "); //bark
					System.exit(1); //die
					break;
			}
		}
		
		//check broadcast requirements
		if(nextState.addFU.outcomeReady && nextState.addFU.cycleCounter == 0){
			//The counter has run out, so we need to broadcast the result
			nextState = nextState.addFU.broadcast(nextState);
			
			//free up the FU so that the next cycle can use it
			nextState.addFU.deallocate();
		}else {
			//counter has not yet run out, so we advance the counter, and will check back next cycle
			nextState.addFU.advanceComputation();
		}
		
		//if the multFU is ready, execute!
		if(!currentState.multFU[0].available && !currentState.multFU[0].outcomeReady){
			nextState.multFU[0].cycleCounter = 6;
			nextState = currentState.multFU[0].performMult(nextState);
		}
		
		//check progress on stage 2
		if(currentState.multFU[2].outcomeReady && currentState.multFU[2].cycleCounter == 0){
			//it has been six cycles, broadcast
			nextState = nextState.multFU[2].broadcast(nextState);
			
			//free up this pipeline slot
			nextState.multFU[2].deallocate();
		} else {
			//otherwise, update the cycle counter
			nextState.multFU[2].advanceComputation();
		}
		
		//check progress on stage 1
		if(currentState.multFU[1].outcomeReady && currentState.multFU[1].cycleCounter == 2){
			//it has been four cycles, move to the next pipeline slot
			nextState.multFU[2] = currentState.multFU[1];
			
			//free up the old pipeline slot
			nextState.multFU[1].deallocate();
		} else {
			//otherwise, update the cycle counter
			nextState.multFU[1].advanceComputation();
		}
		
		//check progress on stage 0
		if(currentState.multFU[0].outcomeReady && currentState.multFU[0].cycleCounter == 4){
			//it has been two cycles, move to the next pipeline slot
			nextState.multFU[1] = currentState.multFU[0];
			
			//free up the old pipeline slot
			nextState.multFU[0].deallocate();
		} else {
			//otherwise, update the cycle counter
			nextState.multFU[0].advanceComputation();
		}
		
		
		//if the memFU is ready, execute!
		if(!currentState.memFU.outcomeReady && !currentState.memFU.available){
			switch(currentState.memFU.opcode){
			case 2: //this is a lw
				nextState.memFU.cycleCounter = 3;
				nextState = currentState.memFU.performLw(nextState);
				break;
			case 3: //this is a sw
				nextState.memFU.cycleCounter = 3;
				nextState = currentState.memFU.performSw(nextState);
				break;
			default: 
				System.out.println("Unexpected opcode in the add FU... "); //bark
				System.exit(1); //die
				break;
			}
		}

		//check broadcast conditions
		if(currentState.memFU.outcomeReady && nextState.memFU.cycleCounter == 0){
			nextState = nextState.memFU.broadcast(nextState);
			
			//free up the FU so that the next cycle can use it
			nextState.memFU.deallocate();
		} else {
			nextState.memFU.advanceComputation();
		}
		
		return nextState;
	}
	
	/***
	 * commit() performs the commit stage of the pipeline. In this stage we do the following:
	 * 1) retire oldest two rob entries
	 * 2) if oldest instruction is not ready, do nothing
	 * 3) if second oldest instruction is not ready, only retire oldest instruction
	 * @param rsBank
	 * @param robHead
	 * @return
	 */
	public static MachineState commit(MachineState currentState, MachineState nextState){
		boolean skipPcIncrement = false;

		//find oldest instruction in the rob
		int oldestIndex = -1;
		for(int i = 0; i < 16; i++){
			oldestIndex = (currentState.robHead + i) % currentState.reorderBuffer.entries.length;
			if(currentState.reorderBuffer.entries[oldestIndex].inUse){
				//oldestIndex is seriously the oldestIndex, so use this one to commit
				break;
			}
		}
	
		//figure out if that instruction is ready to commit
		if(currentState.reorderBuffer.entries[oldestIndex].destRegisterValueValid){
			//commit the instruction at oldestIndex
			skipPcIncrement = nextState.doCommit(oldestIndex, currentState);

		}else{
			//if the oldest isn't ready, we can't commit anything 
			if(!skipPcIncrement){
				//increment pc
				nextState.pc += nextState.fetchPcIncrement;
				nextState.cycles += nextState.fetchPcIncrement;
			}
			
			//return to caller
			return nextState;
		}
		
		//now check the next oldest
		oldestIndex = (oldestIndex + 1) % currentState.reorderBuffer.entries.length;
		//figure out if that instruction is ready to commit
		if(currentState.reorderBuffer.entries[oldestIndex].destRegisterValueValid){
			//if it is, commit the instruction at oldestIndex
			skipPcIncrement = nextState.doCommit(oldestIndex, currentState);
		}
	
		//we don't care about the else in this case, just proceed to the next pipeline stage
		if(!skipPcIncrement){
			//increment pc
			nextState.pc += nextState.fetchPcIncrement;
			nextState.cycles += nextState.fetchPcIncrement;
		}
		
		//return to caller
		return nextState;
	}
	
	/*----------------- utility functions -------------------*/
	
	/**
	 * This routine returns to the schedule routine which RS entry should be placed onto the FU based on the age of the instruction
	 * in the rob
	 * @param rsBank
	 * @param robHead
	 * @return
	 */
	public static int getRSEntryToSchedule(ReservationStationEntry[] rsBank, int robHead){
		//local variables
		int instructionAge; //temp storage for the rob age of the current instruction
		int currentMaxAge = -1; //comparison value used for selecting the maximum age, initialize it to an impossibly low number
		int indexToReturn = -1; //initialize to -1, so that caller will know definitively when there are no ready instructions
		//iterate through the bank and calculate the age of each instruction ready for execution
		for(int i = 0; i < rsBank.length; i++){
			//if both source registers are valid, instruction is ready for scheduling
			if(rsBank[i].source1Valid && rsBank[i].source2Valid){
				instructionAge = robHead - rsBank[i].destReg;
				
				//if the age is negative using subtraction, 
				//	we have to calculate it by adding from the instruction to the end of the rob plus the current rob position
				if(instructionAge < 0){
					instructionAge = robHead + (16 - rsBank[i].destReg);
				} 
				
				//interchange return index if we have found an older instruction
				if(instructionAge > currentMaxAge){
					indexToReturn = i;
					currentMaxAge = instructionAge;
				}
			}
		}

		return indexToReturn;
	}
	
	/**
	 * This is a unit test handle. Currently it ensures that the common utility class is working appropriately. In the future should be expanded to full features
	 */
	public void testSimulator(){
		Boolean commonUtilityPassed = CommonUtility.commonUtilityPassedUnitTest();
		if(commonUtilityPassed){
			System.out.println("All systems are go!");
		}
	}
}
