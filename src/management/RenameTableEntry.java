package management;

public class RenameTableEntry {
	public Boolean valid = false;
	public int renameIndex = -1;
	
	//copy constructor
	public RenameTableEntry(RenameTableEntry sourceRTE){
		this.valid = sourceRTE.valid;
		this.renameIndex = sourceRTE.renameIndex;
	}
	
	//default constructor
	public RenameTableEntry(){
		this.valid = false;
		this.renameIndex = -1;
	}
}
