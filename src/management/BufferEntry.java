package management;

public class BufferEntry {
	public int instruction = 0;
	public Boolean valid = false;
	public Boolean branchPredictedTaken = false;
	
	public int sourceReg1;
	public int sourceReg2;
	
	//we need a record of the pc when this instruction was fetched
	//	we are using this reference for branch targeting, so we really need pc + 1
	public int pcPlusOne;
	
	//copy constructor
	public BufferEntry(BufferEntry sourceBE){
		this.instruction = sourceBE.instruction;
		this.valid = sourceBE.valid;
		this.branchPredictedTaken = sourceBE.branchPredictedTaken;
		this.pcPlusOne = sourceBE.pcPlusOne;
		
		this.sourceReg1 = sourceBE.sourceReg1;
		this.sourceReg2 = sourceBE.sourceReg2;
	}
	
	public BufferEntry(){
		this.instruction = 0;
		this.valid = false;
		this.branchPredictedTaken = false;
		this.pcPlusOne = 0;
		
		this.sourceReg1 = -1;
		this.sourceReg2 = -1;
	}
}
