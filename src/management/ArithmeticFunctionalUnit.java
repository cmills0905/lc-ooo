package management;

public class ArithmeticFunctionalUnit {
	public int sourceValue1;
	public int sourceValue2;
	public int outcome;
	public int robIndex;
	public int cycleCounter;
	public Boolean available;
	public Boolean outcomeReady;
	public Boolean eq;
	public int opcode;
	
	//these are really only for BEQ, but we want to reuse code as much as possible
	public boolean branchPredictedTaken;
	public boolean branchTaken;
	public int branchTarget;
	public int pcPlusOne;
	
	//copy constructor
	public ArithmeticFunctionalUnit(ArithmeticFunctionalUnit sourceAFU){
		this.sourceValue1 = sourceAFU.sourceValue1;
		this.sourceValue2 = sourceAFU.sourceValue2;
		this.outcome = sourceAFU.outcome;
		this.robIndex = sourceAFU.robIndex;
		this.cycleCounter = sourceAFU.cycleCounter;
		this.available = sourceAFU.available;
		this.outcomeReady = sourceAFU.outcomeReady;
		this.eq = sourceAFU.eq;
		this.opcode = sourceAFU.opcode;
		
		this.branchTaken = sourceAFU.branchTaken;
		this.branchTarget = sourceAFU.branchTarget;
		
		this.pcPlusOne = sourceAFU.pcPlusOne;
	}
	
	//default constructor
	public ArithmeticFunctionalUnit(){
		this.sourceValue1 = 0;
		this.sourceValue2 = 0;
		this.outcome = 0;
		this.robIndex = -1;
		this.cycleCounter = 1;
		this.available = true;
		this.outcomeReady = false;
		this.eq = true;
		this.opcode = OpCodes.NOOP.ordinal();
		
		this.branchPredictedTaken = false;
		this.branchTaken = false;
		this.branchTarget = 0;
		
		this.pcPlusOne = 0;
	}
	
	/**
	 * Performs an add and stores the result into the appropriate FU entry
	 * @param fs
	 * @return
	 */
	public ArithmeticFunctionalUnit performAdd(ArithmeticFunctionalUnit fs){
		fs.outcome = (this.sourceValue1 + this.sourceValue2);
		System.out.println("Adding: " + this.sourceValue1 + " + " + this.sourceValue2 + " = " + fs.outcome);
		fs.outcomeReady = true;
		fs.advanceComputation();
		return fs;
	}
	
	/**
	 * Performs a nand and stores the result into the appropriate FU entry
	 * @param fs
	 * @return
	 */
	public ArithmeticFunctionalUnit performNand(ArithmeticFunctionalUnit fs){
		fs.outcome = ~(this.sourceValue1 & this.sourceValue2);
		fs.outcomeReady = true;
		fs.advanceComputation();
		
		return fs;
	}
	
	/**
	 * Performs a mult and stores the result into the appropriate FU entry
	 * @param ms
	 * @return
	 */
	public MachineState performMult(MachineState ms){
		ms.multFU[0].outcome = (this.sourceValue1 * this.sourceValue2);
		ms.multFU[0].outcomeReady = true;
		advanceComputation();
		
		return ms;
	}
	
	/**
	 * Performs a load and stores the result into the appropriate FU entry
	 * @param ms
	 * @return
	 */
	public MachineState performLw(MachineState newState){
		//store reg + immediate in outcome
		newState.memFU.outcome = this.sourceValue1 + this.sourceValue2;

		//retrieve value at address = reg + immediate and store that as final outcome
		newState.memFU.outcome = newState.dataMemory[newState.memFU.outcome];
	
		//mark outcome as ready
		newState.memFU.outcomeReady = true;
		
		//advance counter
		advanceComputation();
		
		return newState;
	}
	
	/**
	 * Performs a store and stores the result into the appropriate FU entry
	 * @param ms
	 * @return
	 */
	public MachineState performSw(MachineState newState){
		//store reg + immediate in outcome
		newState.memFU.outcome = this.sourceValue1 + this.branchTarget;
		newState.memFU.outcomeReady = true;

		//push source 2 to that address in data memory
		newState.dataMemory[newState.memFU.outcome] = newState.memFU.sourceValue2;		
		
		newState.reorderBuffer.entries[this.robIndex] = new ReorderBufferEntry();
		//prepare to return to caller.
		newState.addFU.outcomeReady = true;
		newState.addFU.advanceComputation();
		
		return newState;
	}
	
	/**
	 * Performs a branch and stores the result into the appropriate FU entry
	 * @param ms
	 * @return
	 */
	public MachineState performBeq(MachineState newState){
		//compare the two registervalues
		//	if they are equal, we need to update the taken flag and the target value
		if(newState.addFU.sourceValue1 == newState.addFU.sourceValue2){
			
			// if the values are equal, take the branch
			newState.addFU.branchTaken = true;
			
			//target in-bound is just the offset, we need to add PC + 1 to the offset and send that value back as the branchTarget
			//	this is an OK thing to do, because if this isn't a branch, we aren't using those fields anyway.
			//	We just need to be able to distinguish between an offset and a register value
			newState.addFU.branchTarget = newState.addFU.branchTarget + newState.addFU.pcPlusOne;
			newState.branches++;
		} else{
			newState.addFU.branchTaken = false;
		}
		
		//prepare to return to caller.
		newState.addFU.outcomeReady = true;
		newState.addFU.advanceComputation();
		
		return newState;
	}
	
	/**
	 * sends the results from this functional unit to the rob and reservation stations and makes sure the rename table is current
	 * @param newState
	 * @return
	 */
	public MachineState broadcast(MachineState newState){
		//if the opcode is sw, nothing to do, because the sw instruction is atomic inside of execute.
		//When execute occurs for sw, the dataMemory is updated in one step.
		if(opcode == OpCodes.SW.ordinal()){
			return newState;
		}
		
		//likewise, if the robIndex is -1, then there is nothing to update
		if(this.robIndex < 0){
			return newState;
		}
		
		//if this is not a BEQ, we need to update the dest reg in the ROB as well as all RSes
		if(opcode != OpCodes.BEQ.ordinal()){
			//update the rob
			newState.reorderBuffer.entries[this.robIndex].destRegisterValue= this.outcome;
			newState.reorderBuffer.entries[this.robIndex].destRegisterValueValid = true;
			
			//update the addRS
			for(int i = 0; i < newState.addRS.length; i++){
				//if the source1 valid bit is not set AND the register index matches the rob index, then update the value
				if(!newState.addRS[i].source1Valid && (newState.addRS[i].source1Reg == this.robIndex)){
					newState.addRS[i].source1Value = this.outcome;
					newState.addRS[i].source1Valid = true;
				}
				
				//if the source2 valid bit is not set AND the register index matches the rob index, then update the value
				if(!newState.addRS[i].source2Valid && (newState.addRS[i].source2Reg == this.robIndex)){
					newState.addRS[i].source2Value = this.outcome;
					newState.addRS[i].source2Valid = true;
				}
			}
			
			//update the memRS
			for(int i = 0; i < newState.memRS.length; i++){
				//if the source1 valid bit is not set AND the register index matches the rob index, then update the value
				if(!newState.memRS[i].source1Valid && (newState.memRS[i].source1Reg == this.robIndex)){
					newState.memRS[i].source1Value = this.outcome;
					newState.memRS[i].source1Valid = true;
				}
				
				//if the source2 valid bit is not set AND the register index matches the rob index, then update the value
				if(!newState.memRS[i].source2Valid && (newState.memRS[i].source2Reg == this.robIndex)){
					newState.memRS[i].source2Value = this.outcome;
					newState.memRS[i].source2Valid = true;
				}
			}
			
			//update the multRS
			for(int i = 0; i < newState.multRS.length; i++){
				//if the source1 valid bit is not set AND the register index matches the rob index, then update the value
				if(!newState.multRS[i].source1Valid && (newState.multRS[i].source1Reg == this.robIndex)){
					newState.multRS[i].source1Value = this.outcome;
					newState.multRS[i].source1Valid = true;
				}
				
				//if the source2 valid bit is not set AND the register index matches the rob index, then update the value
				if(!newState.multRS[i].source2Valid && (newState.multRS[i].source2Reg == this.robIndex)){
					newState.multRS[i].source2Value = this.outcome;
					newState.multRS[i].source2Valid = true;
				}
			}
		}else{
			//if this was a beq, update the ROB target so that the pc can be updated in commit.
			newState.reorderBuffer.entries[this.robIndex].branchTarget = this.branchTarget;
			newState.reorderBuffer.entries[this.robIndex].destRegisterValueValid = true; //this is a bit of a misnomer, but this flag lets us know that the ROB is ready for commit.
			newState.reorderBuffer.entries[this.robIndex].branchTaken = this.branchTaken;
		}
		
		//update the rename table
		int tempIndex = this.robIndex;
		int destRegId = newState.reorderBuffer.entries[this.robIndex].destRegisterId;
		if(destRegId > 0){
			for(int i = 1; i < 16; i++){
				tempIndex = (this.robIndex - i);
				//System.out.println("temp step 1: " + tempIndex);
				if(tempIndex < 0){
					tempIndex = 16 - Math.abs(tempIndex);
				}

				//check to see if this rob entry is trying to write to the same dest register
				if(newState.reorderBuffer.entries[tempIndex].destRegisterId == destRegId && !newState.reorderBuffer.entries[tempIndex].destRegisterValueValid){
					newState.renameTable.entries[destRegId].renameIndex = tempIndex;
					newState.renameTable.entries[destRegId].valid = true;
				} else{
					newState.renameTable.entries[destRegId].renameIndex = -1;
					newState.renameTable.entries[destRegId].valid = false;
				}
			}
		}
		
		return newState;
	}

	/**
	 * routine to deallocate a FU. This ensures that the scheduling flags are set correctly, whereas there is no such guarantee using the default constructor
	 */
	public void deallocate(){
		this.opcode = OpCodes.NOOP.ordinal();
		this.available = true;
		this.outcomeReady = false;
	}
	public void advanceComputation(){
		this.cycleCounter--;
	}
}
