package management;

public class BranchPredictor {

	public BranchTargetBufferEntry btb[];
	public int bht[];
	
	//copy constructor
	public BranchPredictor(BranchPredictor bp){
		for(int i = 0; i < bp.btb.length; i++){
			this.btb[i] = bp.btb[i];
		}
		
		for(int i = 0; i < bp.bht.length; i++){
			this.bht[i] = bp.bht[i];
		}
	}
	
	//default constructor
	public BranchPredictor(int size){
		this.btb = new BranchTargetBufferEntry[size];
		for(int i = 0; i < btb.length; i++){
			this.btb[i] = new BranchTargetBufferEntry();
		}
		
		this.bht = new int[size];
		for(int i = 0; i < bht.length; i++){
			this.bht[i] = 1;
		}
	}
	
	/**
	 * First checks the branch target buffer to see if this branch is already installed. 
	 * If it is not, then the branch is installed with an initial state of 1: weakly not taken.
	 * Else nothing happens.
	 * @param bpc
	 * @param instruction
	 */
	public void tryInstall(int bpc, int instruction){
		System.out.println("Installing the branch!");
		int index = (instruction & 0x0000003F);
		//does this already exist
		System.out.println("Checking " + this.btb[index].bpc + " against " + bpc);
		if(this.btb[index].bpc == bpc){
			System.out.println("Already installed... nothing to do...");
			//already installed, return to caller
			return;
		} else {
			System.out.println("Installing  " + CommonUtility.getInstructionString(instruction) + " to " + index);
			System.out.println("Offset: " + CommonUtility.getImmediate(instruction));
			this.btb[index].target = (CommonUtility.getImmediate(instruction) + bpc + 1);
			System.out.println("TARGET: " + this.btb[index].target);
			this.btb[index].bpc = bpc;
			System.out.println("BPC: " + this.btb[index].bpc);
			//can't be locked because we just installed it!
			this.btb[index].lock = false;
			
			//set up branch history
			this.bht[index] = 1;
		}
	}
	
	/**
	 * Takes an instruction, looks that instruction's state up in the branch history table and based on that state predicts
	 * whether or not the branch will be taken.
	 * @param instruction
	 * @return
	 */
	public Boolean predict(int instruction){
		//get the bpc
		int index = instruction & 0x0000003F;
		
		int state = this.bht[index];
		if(state == 0 || state == 1){
			return false;
		} else if(state == 2 || state == 3) {
			return true;
		} 
		
		System.out.println("Branch history table entry is in an unexpected state... predicting not taken as a default.");
		return false;
		
	}
	
	/**
	 * easy handle to get the target for an instruction in the btb, should be used when the branch is known to be installed.
	 * @param instruction
	 * @return
	 */
	public int getTarget(int instruction){
		return  this.btb[instruction & 0x0000003F].target;
	}
	
	/**
	 * This updates the FSM that determines the branch prediction of subsequent calls to predict()
	 * @param instruction
	 * @param input
	 */
	public void updatePredictionState(int instruction, int input){

		int index = (instruction & 0x0000003F);
		//get the current state:
		int state = this.bht[index];
		System.out.println("bht[" + index + "] =" + state);
		//update state
		switch(state){
		case 0:
			if(input == 0){
				state = 0;
			}else{
				state = 1;
			}
			break;
		case 1:
			if(input == 0){
				state = 0;
			} else {
				state = 2;
			}
			break;
		case 2:
			if(input == 0){
				state = 1;
			} else {
				state = 3;
			}
			break;
		case 3:
			if(input == 0){
				state = 2;
			} else {
				state = 3;
			}
			break;
		}
		//save updated state
		this.bht[index] = state;
		
	}
}
