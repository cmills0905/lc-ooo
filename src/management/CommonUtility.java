package management;

public class CommonUtility {
	public static int getOpCode(int instr){
		//Note: it is assumed that bits 31-25 are unused for all instructions
		return instr >> 22; //get rid of everything to the right and return
	}
	
	public static int getField0(int instr){
		return (instr>>19) & 0x7; 
	}
	
	public static int getField1(int instr){
		return (instr>>16) & 0x7;
	}
	
	public static int getField2(int instr){
		return instr & 0xFFFF;
	}
	
	public static int getTwosComplement(int val){
		if(val >> 15 == 0){
			return val;
		}
		return -(~(short)val + 1);
	}
	
	public static int getImmediate(int instr){
		return(getTwosComplement(getField2(instr)));
	}
	
	public static String getOpCodeString(int opcode){
		switch(opcode){
		case 0:
			return "add";
		case 1:
			return "nand";
		case 2:
			return "lw";
		case 3:
			return "sw";
		case 4:
			return "beq";
		case 5:
			return "mult";
		case 6:
			return "halt";
		case 7:
			return "noop";
		default:
			return "data";
	}
	}
	
	public static String getInstructionString(int instr){
		String opCodeString = getOpCodeString(getOpCode(instr));
		
		if(opCodeString.equalsIgnoreCase("beq") || opCodeString.equalsIgnoreCase("sw") || opCodeString.equalsIgnoreCase("lw")){
			return opCodeString + " " + getField0(instr) + " " + getField1(instr) + " " + getImmediate(instr);
		} else{
			return opCodeString + " " + getField0(instr) + " " + getField1(instr) + " " + getField2(instr);
		}
	}
	
	/* Begin unit testing */
	
	public static Boolean testGetOpCode(){
		if(getOpCode(655361) != 0){
			System.out.println("CommonUtility.getOpCode() fails for add");
			return false;
		}
		if(getOpCode(5373956) != 1){
			System.out.println("CommonUtility.getOpCode() fails for nand");
			return false;
		}
		if(getOpCode(8454151) != 2){
			System.out.println("CommonUtility.getOpCode() fails for lw");
			return false;
		}
		if(getOpCode(13893642) != 3){
			System.out.println("CommonUtility.getOpCode() fails for sw");
			return false;
		}
		if(getOpCode(16842754) != 4){
			System.out.println("CommonUtility.getOpCode() fails for beq");
			return false;
		}
		if(getOpCode(22151171) != 5){
			System.out.println("CommonUtility.getOpCode() fails for mult");
			return false;
		}
		if(getOpCode(25165824) != 6){
			System.out.println("CommonUtility.getOpCode() fails for halt");
			return false;
		}
		if(getOpCode(29360128) != 7){
			System.out.println("CommonUtility.getOpCode() fails for noop");
			return false;
		}
		
		return true;
	
	}
	
	public static Boolean testGetField0(){
		if(getField0(655361) != 1){
			System.out.println("CommonUtility.GetField0() failed on add.");
			System.out.println("Returned: " + getField0(655361) + " Expected: 1");
			return false;
		}
		if(getField0(22151171) != 2){
			System.out.println("CommonUtility.GetField0() failed on mult.");
			System.out.println("Returned: " + getField0(22151171) + " Expected: 2");
			return false;
		}
		if(getField0(13893642) != 2){
			System.out.println("CommonUtility.GetField0() failed on sw.");
			System.out.println("Returned: " + getField0(13893642) + " Expected: 2");
			return false;
		}
		return true;
	}

	public static Boolean testGetField1(){
		if(getField1(655361) != 2){
			System.out.println("CommonUtility.GetField1() failed on add.");
			System.out.println("Returned: " + getField1(655361) + " Expected: 2");
			return false;
		}
		if(getField1(22151171) != 2){
			System.out.println("CommonUtility.GetField1() failed on mult.");
			System.out.println("Returned: " + getField1(22151171) + " Expected: 2");
			return false;
		}
		if(getField1(13893642) != 4){
			System.out.println("CommonUtility.GetField1() failed on sw.");
			System.out.println("Returned: " + getField1(13893642) + " Expected: 4");
			return false;
		}
		return true;
	}
	
	public static Boolean testGetField2(){
		if(getField2(655361) != 1){
			System.out.println("CommonUtility.GetField2() failed on add.");
			System.out.println("Returned: " + getField2(655361) + " Expected: 1");
			return false;
		}
		if(getField2(22151171) != 3){
			System.out.println("CommonUtility.GetField2() failed on mult.");
			System.out.println("Returned: " + getField2(22151171) + " Expected: 3");
			return false;
		}
		return true;
	}
	
	public static Boolean testGetImmediate(){
		if(getImmediate(13107215) != 15){
			System.out.println("CommonUtility.GetImmediate() failed on sw (positive case)");
			System.out.println("Returned: " + getImmediate(13107215) + " Expected: 15");
			return false;
		}
		if(getImmediate(17367037) != -3){
			System.out.println("CommonUtility.GetImmediate() failed on beq (negative case)");
			System.out.println("Returned: " + getImmediate(17367037) + " Expected: -3");
			return false;
		}
		return true;
	}
	
	public static Boolean testGetTwosComplement(){
		if(getTwosComplement(5) != 5){
			System.out.println("getTwosComplement() failed for 5");
			System.out.println("Returned: " + getTwosComplement(5) + " Expected: 5");
			return false;
		}
		if(getTwosComplement(-5) != -5){
			System.out.println("getTwosComplement() failed for -5");
			System.out.println("Returned: " + getTwosComplement(-5) + " Expected: -5");
			return false;
		}
		if(getTwosComplement(-50) != -50){
			System.out.println("getTwosComplement() failed for -50");
			System.out.println("Returned: " + getTwosComplement(-50) + " Expected: -50");
			return false;
		}
		return true;
	}

	public static Boolean testGetInstructionString(){
		if(!getInstructionString(22151171).equals("mult 2 2 3")){
			System.out.println("ComonUtility.getInstructionString failed...");
			System.out.println("Returned: '" + getInstructionString(22151171) + "'");
			System.out.println("Expected: 'mult 2 2 3'");
			return false;
		}
		if(!getInstructionString(13893642).equals("sw 2 4 10")){
			System.out.println("ComonUtility.getInstructionString failed...");
			System.out.println("Returned: '" + getInstructionString(13893642) + "'");
			System.out.println("Expected: 'sw 2 4 10'");
			return false;
		}
		return true;
	}
	
	public static Boolean commonUtilityPassedUnitTest(){
		if(!testGetOpCode()){
			System.out.println("getOpCode() failed unit test...");
			return false;
		}
		if(!testGetField0()){
			System.out.println("getField0() failed unit test...");
			return false;
		}
		if(!testGetField1()){
			System.out.println("getField1() failed unit test...");
			return false;
		}
		if(!testGetField2()){
			System.out.println("getField2() failed unit test...");
			return false;
		}
		if(!testGetTwosComplement()){
			System.out.println("getTwosComplement() failed unit test...");
			return false;
		}
		if(!testGetImmediate()){
			System.out.println("getImmediate() failed unit test...");
			return false;
		}
		if(!testGetInstructionString()){
			System.out.println("getInstructionString() failed unit test...");
			return false;
		}
		
		System.out.println("All CommonUtility features working correctly!");
		return true;
	}
}
