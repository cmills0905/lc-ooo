package management;

public class RenameTable {
	public RenameTableEntry entries[];
	
	//copy constructor
	public RenameTable(RenameTable sourceRT){
		this.entries = new RenameTableEntry[sourceRT.entries.length];
		for(int i = 0; i < sourceRT.entries.length; i++){
			this.entries[i] = new RenameTableEntry(sourceRT.entries[i]);
		}
	}
	
	//default constructor
	public RenameTable(int numRegs){
		entries = new RenameTableEntry[numRegs];
		for(int i = 0; i < entries.length; i++){
			entries[i] = new RenameTableEntry();
		}
	}
	
	/**
	 * returns the alias of a register
	 * @param reg
	 * @return
	 */
	public int getNewName(int reg){
		if(entries[reg].valid){
			return entries[reg].renameIndex;
		} else {
			return -1;
		}
	}
	
	/**
	 * accepts a buffer entry and returns the same buffer entry with the physical registers aliased by the appropriate rob index
	 * @param be
	 * @return
	 */
	public BufferEntry rename(BufferEntry be){
		int tempLookup;
		int tempInstruction = be.instruction;
		int tempOpCode = CommonUtility.getOpCode(tempInstruction);
		
		//if the instruction is not a noop or a halt, then it has at least one register to rename
		if(tempOpCode != OpCodes.NOOP.ordinal() && tempOpCode != OpCodes.HALT.ordinal()){
			//get the physical register number
			tempLookup = CommonUtility.getField0(tempInstruction); 

			//store the new name of the first register
			be.sourceReg1 = this.getNewName(tempLookup);

		}
		
		//the following types of instructions also have a second instruction that should be renamed
		if(tempOpCode == OpCodes.BEQ.ordinal() || tempOpCode == OpCodes.ADD.ordinal() || tempOpCode == OpCodes.NAND.ordinal() || tempOpCode == OpCodes.MULT.ordinal() || tempOpCode == OpCodes.SW.ordinal()){
			//get the physical register number
			tempLookup = CommonUtility.getField1(tempInstruction); 

			//store the new name of the second
			be.sourceReg2 = this.getNewName(tempLookup);

		}
		
		return be;
	}
	
}
