package management;

public class ReservationStationEntry {
	public int source1Reg;
	public int source2Reg;
	public int destReg;
	
	public int source1Value;
	public int source2Value;
	
	public Boolean source1Valid;
	public Boolean source2Valid;
	
	public int branchOffset;
	//pcPlusOne is sent up to calculate the branch target at execute. the RS needs this info to pass to the FU
	public int pcPlusOne;
	
	public int opcode;
	
	//ONLY FOR DEBUG
	public int instruction;
	
	//copy constuctor
	public ReservationStationEntry(ReservationStationEntry sourceRSE){
		this.source1Reg = sourceRSE.source1Reg;
		this.source2Reg = sourceRSE.source2Reg;
		this.destReg = sourceRSE.destReg;
		this.source1Value = sourceRSE.source1Value;
		this.source2Value = sourceRSE.source2Value;
		this.source1Valid = sourceRSE.source1Valid;
		this.source2Valid = sourceRSE.source2Valid;
		this.opcode = sourceRSE.opcode;
		
		this.branchOffset = sourceRSE.branchOffset;
		
		this.pcPlusOne = sourceRSE.pcPlusOne;
		
		this.instruction = sourceRSE.instruction;
	}
	
	//default constructor
	public ReservationStationEntry(){
		this.source1Reg = -1;
		this.source2Reg = -1;
		this.destReg = -1;
		this.source1Value = 0;
		this.source2Value = 0;
		this.source1Valid = false;
		this.source2Valid = false;
		this.opcode = OpCodes.NOOP.ordinal();
		
		this.instruction = 0;
		
		this.branchOffset = -1;
		
		this.pcPlusOne = 0;
	}
	
	//call this method to free up the reservation station
	public void deallocate(){
		this.opcode = OpCodes.NOOP.ordinal(); //set opcode to NOOP
		this.instruction = 0;
		this.source1Valid = false; //invalidate source 1
		this.source2Valid = false; //invalidate source 2
	}
}
