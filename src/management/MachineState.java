package management;

public class MachineState{
	private int MAX_MEM = 65536;
	private int FETCH_PER_CYCLE = 2;
	private int NUM_REGISTERS = 8;
	private int NUM_ROB_ENTRIES = 16;
	private int RES_STATION_SIZE = 3;
	private int BRANCH_PREDICTOR_SIZE = 64;
	
	// ----- Statistics ----- //
	public int cycles;
	public int fetched;
	public int retired;
	public int branches;
	public int mispred;
	
	//pc is the program counter for the machine and keeps track of code path during execution
	public int pc = 0;
	
	//instruction memory tail keeps us from fetching an infinite number of ridiculous instructions and filling the rob up with them.
	public int instructionMemoryTail = 0;
	//branchPredictor is the implementation of a branch predictor for the system
	public BranchPredictor branchPredictor;
	
	//instruction memory holds the instructions that make up the program being executed
	public int instructionMemory[] = new int[MAX_MEM];
	
	//data memory holds the instructions in the instruction memory in addition to any data stored into memory during execution
	public int dataMemory[] = new int[MAX_MEM];
	
	//reg holds the register values
	public int reg[] = new int[NUM_REGISTERS];
	
	//fetchBuffer holds the instructions loaded from instruction memory
	public BufferEntry fetchBuffer[] = new BufferEntry[FETCH_PER_CYCLE];
	
	//fetchPcIncrement holds the number of valid instructions fetched from instruction memory so that the pc can be managed correctly
	public int fetchPcIncrement = 0;
	
	//renameTable acts as the rename table implementation for the machine linking in-flight instruction's dest registers
	//	to rob entries writing to those registers
	public RenameTable renameTable;
	
	//renameBuffer holds the instructions taken from the fetchBuffer and attempts to update the renameTable for each valid entry
	public BufferEntry renameBuffer[] = new BufferEntry[FETCH_PER_CYCLE];
	
	//reorderBuffer acts as the rob implementation and holds in-flight instructions which will be scheduled out of order, but committed in program order
	public ReorderBuffer reorderBuffer;
	
	//allocateBuffer holds the instructions carried from renameBuffer
	public BufferEntry allocateBuffer[] = new BufferEntry[FETCH_PER_CYCLE];
	
	//robHead holds the current robHead pointer
	public int robHead;
	
	//robHazard indicates whether or not the rob is full, which will cause a structural hazard.
	public Boolean robHazard;
	
	//addRS acts as the reservation station implementation for the addFU
	public ReservationStationEntry addRS[] = new ReservationStationEntry[RES_STATION_SIZE];
	
	//multRS acts as the reservation station implementation for the multFU
	public ReservationStationEntry multRS[] = new ReservationStationEntry[RES_STATION_SIZE];
	
	//memRS acts as the reservation station implementation for the memFU
	public ReservationStationEntry memRS[] = new ReservationStationEntry[RES_STATION_SIZE];
	
	//addFU serves as the implementation of an addition functional unit
	public ArithmeticFunctionalUnit addFU = new ArithmeticFunctionalUnit();
	
	//multFU serves as the implementation of a multiplication functional unit
	public ArithmeticFunctionalUnit multFU[] = new ArithmeticFunctionalUnit[3];
	
	//memFU serves as the implementation of a memory functional unit
	public ArithmeticFunctionalUnit memFU = new ArithmeticFunctionalUnit();
	
	//copy constructor
	public MachineState(MachineState sourceState){
		
		//copy stats
		this.cycles = sourceState.cycles;
		this.fetched = sourceState.fetched;
		this.retired = sourceState.retired;
		this.branches = sourceState.branches;
		this.mispred = sourceState.mispred;
		
		//copy the pc
		this.pc = sourceState.pc;
		
		this.instructionMemoryTail = sourceState.instructionMemoryTail;
		
		//copy the branch predictor
		this.branchPredictor = sourceState.branchPredictor;
		
		//copy the instruction memory
		for(int i = 0; i < sourceState.instructionMemory.length; i++){
			this.instructionMemory[i] = sourceState.instructionMemory[i];
		}
		//copy the data memory
		for(int i = 0; i < sourceState.dataMemory.length; i++){
			this.dataMemory[i] = sourceState.dataMemory[i];
		}
		
		//copy the register values
		for(int i = 0; i < reg.length; i++){
			this.reg[i] = sourceState.reg[i];
		}
		
		//copy the fetchPcIncrement
		this.fetchPcIncrement = sourceState.fetchPcIncrement;
		
		//copy the fetch buffer
		for(int i = 0; i < sourceState.fetchBuffer.length; i++){
			this.fetchBuffer[i] = new BufferEntry(sourceState.fetchBuffer[i]);
		}
		
		//copy the rename buffer
		for(int i = 0; i < sourceState.renameBuffer.length; i++){
			this.renameBuffer[i] = new BufferEntry(sourceState.renameBuffer[i]);
		}
		
		//copy the allocate buffer
		for(int i = 0; i < sourceState.allocateBuffer.length; i++){
			this.allocateBuffer[i] = new BufferEntry(sourceState.allocateBuffer[i]);
		}
		
		//copy the rename table
		this.renameTable = new RenameTable(sourceState.renameTable);
		
		//copy the reorder buffer
		this.reorderBuffer = new ReorderBuffer(sourceState.reorderBuffer);
		
		//copy the robHead
		this.robHead = sourceState.robHead;
		
		//copy any robHazard
		this.robHazard = sourceState.robHazard;
		
		//copy the addRS
		for(int i = 0; i < addRS.length; i++){
			this.addRS[i] = new ReservationStationEntry(sourceState.addRS[i]);
		}
		
		//copy the multRS
		for(int i = 0; i < multRS.length; i++){
			this.multRS[i] = new ReservationStationEntry(sourceState.multRS[i]);
		}
		
		//copy the memRS
		for(int i = 0; i < memRS.length; i++){
			this.memRS[i] = new ReservationStationEntry(sourceState.memRS[i]);
		}
		
		//copy the addFU
		this.addFU = new ArithmeticFunctionalUnit(sourceState.addFU);
		
		//copy the multFU
		for(int i = 0; i < multFU.length; i++){
			this.multFU[i] = new ArithmeticFunctionalUnit(sourceState.multFU[i]);
		}
		
		//copy the memFU
		this.memFU = new ArithmeticFunctionalUnit(sourceState.memFU);
		
		//copy the RobHazardFlag
	}
	//default constructor
	public MachineState(){
		this.cycles = 0;
		this.fetched = 0;
		this.retired = 0;
		this.branches = 0;
		this.mispred = 0;
		
		this.pc = 0;
		this.instructionMemoryTail = 0;
		this.branchPredictor = new BranchPredictor(BRANCH_PREDICTOR_SIZE);
		this.instructionMemory = new int[MAX_MEM];
		this.dataMemory = new int[MAX_MEM];
		this.reg = new int[NUM_REGISTERS];
		this.fetchPcIncrement = 0;
		
		for(int i = 0; i < fetchBuffer.length; i++){
			this.fetchBuffer[i] = new BufferEntry();
		}
		
		for(int i = 0; i < renameBuffer.length; i++){
			this.renameBuffer[i] = new BufferEntry();
		}
		
		for(int i = 0; i < allocateBuffer.length; i++){
			this.allocateBuffer[i] = new BufferEntry();
		}
		
		//renameTable acts as the rename table implementation for the machine linking in-flight instruction's dest registers
		//	to rob entries writing to those registers
		this.renameTable = new RenameTable(NUM_REGISTERS);
		
		this.reorderBuffer = new ReorderBuffer(NUM_ROB_ENTRIES);
		
		this.robHead = 0;
		
		this.robHazard = false;
		
		//initialize the addRS
		for(int i = 0; i < addRS.length; i++){
			this.addRS[i] = new ReservationStationEntry();
		}
		
		//initialize the multRS
		for(int i = 0; i < multRS.length; i++){
			this.multRS[i] = new ReservationStationEntry();
		}
		
		//initialize the memRS
		for(int i = 0; i < memRS.length; i++){
			this.memRS[i] = new ReservationStationEntry();
		}
		
		//initialize the addFU
		//note the default constructor sets cycle number to 1, which is perfect for this FU
		this.addFU = new ArithmeticFunctionalUnit();
		
		//initialize the multFU
		for(int i = 0; i < multFU.length; i++){
			this.multFU[i] = new ArithmeticFunctionalUnit();
			//the default 1 cycle per computation is insufficient for the multiply FU though, set cycles to 6 explicitly
			this.multFU[i].cycleCounter = 6;
		}
		
		//initialize the memFU
		this.memFU = new ArithmeticFunctionalUnit();
		
	}

	
	/*------------ Display and utility ------------*/
	/**
	 * Encapsulates the modular arithmetic to increment the rob head. 
	 * This is encapsulated primarily to ensure that if the rob size is changed, this calculation is not affected
	 * @return
	 */
	public int getNextRobHead(){
		//the ROB head is a circular buffer, if we return a value equal to the length of the rob, then the simulator is going to explode
		//need to use modular arithmetic to keep this from happening
		return (this.robHead + 1) % this.reorderBuffer.entries.length;
	}
	
	/**
	 * This method is called during miss-prediction recovery, this is a simple way to destroy everything in the machine without using the default constructor.
	 * This allows for future improvements or changes to branch prediction without affecting the rest of the pipeline.
	 */
	public void recoverFromMissedBranch(){
		//reset the robHead
		this.robHead = 0;
		
		//reset the rob
		for(int i = 0; i < this.reorderBuffer.entries.length; i++){
			this.reorderBuffer.entries[i] = new ReorderBufferEntry();
		}
		
		//reset the rename table
		for(int i = 0; i < this.renameTable.entries.length; i++){
			this.renameTable.entries[i] = new RenameTableEntry();
		}
		
		//reset the add reservation stations
		for(int i = 0; i < this.addRS.length; i++){
			this.addRS[i] = new ReservationStationEntry();
		}
		
		//reset the mult reservation stations
		for(int i = 0; i < this.multRS.length; i++){
			this.multRS[i] = new ReservationStationEntry();
		}
		
		//reset the mem reservation stations
		for(int i = 0; i < this.memRS.length; i++){
			this.memRS[i] = new ReservationStationEntry();
		}
		
		//reset the fetch buffer
		for(int i = 0; i < this.fetchBuffer.length; i++){
			this.fetchBuffer[i] = new BufferEntry();
		}
		
		//reset the rename buffer
		for(int i = 0; i < this.renameBuffer.length; i++){
			this.renameBuffer[i] = new BufferEntry();
		}
		
		//reset the allocate buffer
		for(int i = 0; i < this.allocateBuffer.length; i++){
			this.allocateBuffer[i] = new BufferEntry();
		}
		
		//reset the add functional unit
		this.addFU = new ArithmeticFunctionalUnit();
		
		//reset the mem functional unit
		this.memFU = new ArithmeticFunctionalUnit();
		
		//reset the mult functional units
		for(int i = 0; i < this.multFU.length; i++){
			this.multFU[i] = new ArithmeticFunctionalUnit();
		}
		
		//reset the robHazard flag
		this.robHazard = false;
		
		this.mispred++;
	}

	/**
	 * This method is to encapsulate the logic of committing the latest instruction in the rob. The caller passes the oldest index and the method commits the instruction at that
	 * index. The index is not calculated as part of this method because of the way the pipeline is set up. We perform all of the data flow arthmetic (in this case the modular addition
	 * of 1 to the oldest index in a higher scope.
	 * @param oldestIndex
	 * @param currentState
	 * @return
	 */
	public boolean doCommit(int oldestIndex, MachineState currentState){
		boolean skipPcIncrement = false;
		int tempOpCode = CommonUtility.getOpCode(currentState.reorderBuffer.entries[oldestIndex].instruction);
		//time to commit: lw, add, nand or mult
		//this is horribly clunky, but it is here for a reason. We only want to allow a commit from instructions that we know should update
		// this will weed out errant data instructions and any other weirdness from writing to registers inadvertently
		if(tempOpCode == OpCodes.LW.ordinal() || tempOpCode == OpCodes.ADD.ordinal() || tempOpCode == OpCodes.NAND.ordinal() || tempOpCode == OpCodes.MULT.ordinal()){
			this.reg[currentState.reorderBuffer.entries[oldestIndex].destRegisterId] = currentState.reorderBuffer.entries[oldestIndex].destRegisterValue;
		}
		
		//time to commit: beq
		if(tempOpCode == OpCodes.BEQ.ordinal()){
			//if the branch is taken
			if(currentState.reorderBuffer.entries[oldestIndex].branchTaken){
				//update the pc
				this.pc = currentState.reorderBuffer.entries[oldestIndex].branchTarget;
				
				//update the branch prediction state
				this.branchPredictor.updatePredictionState(currentState.reorderBuffer.entries[oldestIndex].instruction, 1);
				//make sure nothing else tampers with the pc
				skipPcIncrement = true;
			} else {
				//update the Predictor
				this.branchPredictor.updatePredictionState(currentState.reorderBuffer.entries[oldestIndex].instruction, 0);
			}
			
			//figure out if the branch was mispredicted
			if(currentState.reorderBuffer.entries[oldestIndex].branchPredictedTaken != currentState.reorderBuffer.entries[oldestIndex].branchTaken){
				//missed a branch... need to recover
				this.recoverFromMissedBranch();
			}
		}
		
		//time to commit: halt
		if(CommonUtility.getOpCode(currentState.reorderBuffer.entries[oldestIndex].instruction) == OpCodes.HALT.ordinal()){
			//time to end the simulation
			this.reorderBuffer.entries[oldestIndex] = new ReorderBufferEntry();
			this.retired++;
			this.printState();
			System.exit(0);
		}
		
		//Note that if this is a noop, we just don't do anything

		//purge the instruction we just committed from the rob. 
		this.reorderBuffer.entries[oldestIndex] = new ReorderBufferEntry();
		this.retired++;
		
		return skipPcIncrement;
	}
	
	/**
	 * This method prints all state values except branch prediction, which is handled in the background. Branch prediction statistics are provided though.
	 */
	public void printState(){
		//print instruction memory
		System.out.println("PC: " + pc);
		System.out.println("Instruction Memory");
		for(int i = 0; i < instructionMemory.length; i++){
			//skip empty memory locations
			if(instructionMemory[i] == 0) {
				continue;
			}
			System.out.println("\tmem[" + i + "] = " + instructionMemory[i]);
		}
		
		//print data memory
		System.out.println("\nData Memory");
		for(int i = 0; i < dataMemory.length; i++){
			//skip empty memory locations
			if(dataMemory[i] == 0){
				continue;
			}
			System.out.println("\tmem[" + i + "] = " + dataMemory[i]);
		}
		
		//print register file
		System.out.println("\nRegister File");
		for(int i = 0; i < reg.length; i++){
			System.out.println("\treg[" + i + "] = " + reg[i]);
		}
		
		//print renameTable
		System.out.println("\nRename Table");
		for(int i = 0; i < renameTable.entries.length; i++){
			System.out.println("\tr[" + i + "] : valid = " + renameTable.entries[i].valid + "\trenameIndex = " + renameTable.entries[i].renameIndex);
		}
		
		//print reorder buffer
		System.out.println("\nReorder Buffer");
		for(int i = 0; i < reorderBuffer.entries.length; i++){
			System.out.println("\trob[" + i + "]: destRegisterId = " + reorderBuffer.entries[i].destRegisterId + "\tdestRegisterValue = " + reorderBuffer.entries[i].destRegisterValue + 
					"\tdestRegisterValueValid = " + reorderBuffer.entries[i].destRegisterValueValid + "\tinstruction = " + CommonUtility.getInstructionString(reorderBuffer.entries[i].instruction));
		}
		
		//print addRS
		System.out.println("\nADD Reservation Station");
		for(int i = 0; i < addRS.length; i++){
			System.out.println(CommonUtility.getInstructionString(addRS[i].instruction));
			System.out.println("\tadd[" + i + "] : src1Reg = " + addRS[i].source1Reg + "\tsrc1Value = " + addRS[i].source1Value + " \tsource1Valid = " + addRS[i].source1Valid);
			System.out.println("\t\t src2Reg = " + addRS[i].source2Reg + "\tsrc2Value = " + addRS[i].source2Value + "\tsrc2Valid = " + addRS[i].source2Valid);
			System.out.println("\t\t destReg = " + addRS[i].destReg + "\toperation = " + CommonUtility.getOpCodeString(addRS[i].opcode));
		}
		
		//print multRS
		System.out.println("\nMULT Reservation Station");
		for(int i = 0; i < multRS.length; i++){
			System.out.println(CommonUtility.getInstructionString(multRS[i].instruction));
			System.out.println("\tmult[" + i + "] : src1Reg = " + multRS[i].source1Reg + "\tsrc1Value = " + multRS[i].source1Value + " \tsource1Valid = " + multRS[i].source1Valid);
			System.out.println("\t\t src2Reg = " + multRS[i].source2Reg + "\tsrc2Value = " + multRS[i].source2Value + "\tsrc2Valid = " + multRS[i].source2Valid);
			System.out.println("\t\t destReg = " + multRS[i].destReg + "\toperation = " + CommonUtility.getOpCodeString(multRS[i].opcode));
		}
		
		//print memRS
		System.out.println("\nMEM Reservation Station");
		for(int i = 0; i < memRS.length; i++){
			System.out.println(CommonUtility.getInstructionString(memRS[i].instruction));
			System.out.println("\tmem[" + i + "] : src1Reg = " + memRS[i].source1Reg + "\tsrc1Value = " + memRS[i].source1Value + " \tsource1Valid = " + memRS[i].source1Valid);
			System.out.println("\t\t src2Reg = " + memRS[i].source2Reg + "\tsrc2Value = " + memRS[i].source2Value + "\tsrc2Valid = " + memRS[i].source2Valid);
			System.out.println("\t\t destReg = " + memRS[i].destReg + "\toperation = " + CommonUtility.getOpCodeString(memRS[i].opcode));
		}
		
		//print addFU
		System.out.println("\nADD Functional Unit");
		System.out.println("ADD Unit: Available = " + addFU.available + " Cycle Counter = " + addFU.cycleCounter + " ROB index = " + addFU.robIndex);
		System.out.println("SourceValue1 = " + addFU.sourceValue1 + " SourceValue2 = " + addFU.sourceValue2 + " Outcome: " + addFU.outcome);
		System.out.println("BRANCH: Predicted Taken = " + addFU.branchPredictedTaken + " Taken = " + addFU.branchTaken + " Target = " + addFU.branchTarget + " Ready: " + addFU.outcomeReady);

		//print multFU
		System.out.println("\nMULT Functional Unit 0");
		System.out.println("MULT Unit 0: Available = " + multFU[0].available + " Cycle Counter = " + multFU[0].cycleCounter + " ROB index = " + multFU[0].robIndex);
		System.out.println("SourceValue1 = " + multFU[0].sourceValue1 + " SourceValue2 = " + multFU[0].sourceValue2 + " Outcome: " + multFU[0].outcome);
		System.out.println("BRANCH: Predicted Taken = " + multFU[0].branchPredictedTaken + " Taken = " + multFU[0].branchTaken + " Target = " + multFU[0].branchTarget + " Ready: " + multFU[0].outcomeReady);

		System.out.println("\nMULT Functional Unit 1");
		System.out.println("MULT Unit 1: Available = " + multFU[1].available + " Cycle Counter = " + multFU[1].cycleCounter + " ROB index = " + multFU[1].robIndex);
		System.out.println("SourceValue1 = " + multFU[1].sourceValue1 + " SourceValue2 = " + multFU[1].sourceValue2 + " Outcome: " + multFU[1].outcome);
		System.out.println("BRANCH: Predicted Taken = " + multFU[1].branchPredictedTaken + " Taken = " + multFU[1].branchTaken + " Target = " + multFU[1].branchTarget + " Ready: " + multFU[1].outcomeReady);

		System.out.println("\nMULT Functional Unit 2");
		System.out.println("MULT Unit 0: Available = " + multFU[2].available + " Cycle Counter = " + multFU[2].cycleCounter + " ROB index = " + multFU[2].robIndex);
		System.out.println("SourceValue1 = " + multFU[2].sourceValue1 + " SourceValue2 = " + multFU[2].sourceValue2 + " Outcome: " + multFU[2].outcome);
		System.out.println("BRANCH: Predicted Taken = " + multFU[2].branchPredictedTaken + " Taken = " + multFU[2].branchTaken + " Target = " + multFU[2].branchTarget + " Ready: " + multFU[2].outcomeReady);

		//print memFU
		System.out.println("\nMEM Functional Unit");
		System.out.println("MEM Unit: Available = " + memFU.available + " Cycle Counter = " + memFU.cycleCounter + " ROB index = " + memFU.robIndex);
		System.out.println("SourceValue1 = " + memFU.sourceValue1 + " SourceValue2 = " + memFU.sourceValue2 + " Outcome: " + memFU.outcome + " Ready: " + memFU.outcomeReady);
		
		//print divider
		System.out.println("\n--------------------------- Pipeline Buffers ---------------------------");
		
		//print fetchBuffer
		System.out.println("\nFetch Buffer");
		for(int i = 0; i < fetchBuffer.length; i++){
			System.out.println("\t bufferEntry[" + i + "].instruction = " + CommonUtility.getInstructionString(fetchBuffer[i].instruction));
			System.out.println("\t\tbufferEntry[" + i + "].valid = " + fetchBuffer[i].valid + "\tbufferEntry[" + i + "].branchToBeTaken = " + fetchBuffer[i].branchPredictedTaken);
		}
		
		//print renameBuffer
		System.out.println("\nRename Buffer");
		for(int i = 0; i < renameBuffer.length; i++){
			System.out.println("\t bufferEntry[" + i + "].instruction = " + CommonUtility.getInstructionString(renameBuffer[i].instruction));
			System.out.println("\t\tbufferEntry[" + i + "].valid = " + renameBuffer[i].valid + "\tbufferEntry[" + i + "].branchToBeTaken = " + renameBuffer[i].branchPredictedTaken);
		}
		
		//print renameBuffer
		System.out.println("\nAllocate Buffer");
		for(int i = 0; i < allocateBuffer.length; i++){
			System.out.println("\t bufferEntry[" + i + "].instruction = " + CommonUtility.getInstructionString(allocateBuffer[i].instruction));
			System.out.println("\t\tbufferEntry[" + i + "].valid = " + allocateBuffer[i].valid + "\tbufferEntry[" + i + "].branchToBeTaken = " + allocateBuffer[i].branchPredictedTaken);
		}
		
		System.out.println("\n");
		System.out.println("--------------------------- System Statistics ---------------------------\n");
		System.out.println("Cycles: " + cycles);
		System.out.println("Fetched: " + fetched);
		System.out.println("Retired: " + retired);
		System.out.println("Branches: " + branches);
		System.out.println("Mispred: " + mispred);
		System.out.println("\n");
	}
}
