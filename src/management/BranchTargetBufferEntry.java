package management;

public class BranchTargetBufferEntry {

	public int bpc;
	public int target;
	public boolean lock;
	
	//clone constructor
	public BranchTargetBufferEntry(BranchTargetBufferEntry btbe){
		this.bpc = btbe.bpc;
		this.target = btbe.target;
		this.lock = btbe.lock;
	}
	
	//default constructor
	public BranchTargetBufferEntry(){
		this.bpc = 0;
		this.target = 0;
		this.lock = false;
	}
}
