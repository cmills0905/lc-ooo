package management;

public enum OpCodes {
	ADD,
	NAND,
	LW,
	SW,
	BEQ,
	MULT,
	HALT,
	NOOP
}
