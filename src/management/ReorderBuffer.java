package management;

public class ReorderBuffer {
	public ReorderBufferEntry entries[];
	
	//copy constructor
	public ReorderBuffer(ReorderBuffer sourceRob){
		this.entries = new ReorderBufferEntry[sourceRob.entries.length];
		for(int i = 0; i < sourceRob.entries.length; i++){
			this.entries[i] = new ReorderBufferEntry(sourceRob.entries[i]);
		}
	}
	//default constructor
	public ReorderBuffer(int numRobEntries){
		entries = new ReorderBufferEntry[numRobEntries];
		for(int i = 0; i < numRobEntries; i++){
			entries[i] = new ReorderBufferEntry();
		}
	}
	
	/**
	 * Takes a register and then returns the rob index of the rob entry (with lowest index) modifying that register.
	 * @param destReg
	 * @return
	 */
	public int getReorderBufferIndex(int destReg){
		//search for matching destReg in all entries in the rob
		for(int i = 0; i < entries.length; i++){
			//if we find a match
			if(entries[i].destRegisterId == destReg){
				//return the index of the match
				return i;
			}
		}
		//if we don't find a match, return a -1 to indicate the destReg is not being modified in the rob
		return -1;
	}
	
	/**
	 * This encapsulates the logic for taking rob entries and putting them into the reservation stations. 
	 * checks to ensure that the following conditions are met:
	 * 1) appropriate opcode for specified RS (add, lw(mem), mult)
	 * 2) not already installed in the rs
	 * 3) cannot be illegally writing a value to the 0 register
	 * 
	 * @param opcode
	 * @param robHead
	 * @return
	 */
	//find eligible rob entry for allocation
	public int getReorderBufferEntryToInstall(OpCodes opcode, int robHead){
		int index;
		//look through entries in the rob
		for(int i = 0; i < entries.length; i++){
			//calculate the index
			index = (robHead + i) % entries.length;
			//System.out.println("Index: " + index);
			//if we are installing into the addRS
			if(opcode == OpCodes.ADD){
				//to be eligible, the entry must be an add, nand or beq
				if(CommonUtility.getOpCode(entries[index].instruction) == OpCodes.ADD.ordinal() 
						|| CommonUtility.getOpCode(entries[index].instruction) == OpCodes.NAND.ordinal()
						|| CommonUtility.getOpCode(entries[index].instruction) == OpCodes.BEQ.ordinal()) {
					//to be eligible, the entry must also not be already installed
					if(!entries[index].installedToRs){
						
						//also can't attempt to write to the 0 register
						if(CommonUtility.getOpCode(entries[index].instruction) != OpCodes.BEQ.ordinal() && CommonUtility.getField2(entries[index].instruction) != 0){
							//at this point, we have an entry with a compatible opcode that has not yet been installed
							//	we are iterating from the beginning of the rob to the current robHead position, so we don't have to 
							//	worry about age. This is our guy.
							return index; //return the index of the rob that needs to be scheduled
						} else if(CommonUtility.getOpCode(entries[index].instruction) == OpCodes.BEQ.ordinal()) {
							System.out.println("RETURNING A BEQ");
							//if it is a BEQ, then we don't care what the field 2 value is because it will never be writing there anyway.
							return index;
						}
					}
				}
			} else if(opcode == OpCodes.MULT){
				//if we are installing into the multRS
				//to be eligible, the entry must be a mult
				if(CommonUtility.getOpCode(entries[index].instruction) == OpCodes.MULT.ordinal()){
					//to be eligible, the entry must also not be already installed
					if(!entries[index].installedToRs){
						//at this point, we have an entry with a compatible opcode that has not yet been installed
						//	we are iterating from the beginning of the rob to the current robHead position, so we don't have to 
						//	worry about age. This is our guy.
						return index; //return the index of the rob that needs to be scheduled
					}
				}
				
			} else {
				//if we are installing into the memRS
				//to be eligible, the entry must be an sw or lw
				if(CommonUtility.getOpCode(entries[index].instruction) == OpCodes.SW.ordinal()
						|| CommonUtility.getOpCode(entries[index].instruction) == OpCodes.LW.ordinal()){
					//to be eligible, the entry must also not be already installed
					if(!entries[index].installedToRs){
						//at this point, we have an entry with a compatible opcode that has not yet been installed
						//	we are iterating from the beginning of the rob to the current robHead position, so we don't have to 
						//	worry about age. This is our guy.
						return index; //return the index of the rob that needs to be scheduled
					}
				}
			}
		}
		//woah, no entries waiting in the ROB... do nothing
		return -1;
	}
}
