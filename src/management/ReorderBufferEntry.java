package management;

public class ReorderBufferEntry {
	public int destRegisterId = -1; //architected register to update
	public int destRegisterValue = -1; //value to update with
	public Boolean destRegisterValueValid = false; //is the value currently valid
	public int instruction = 0; //instruction (for scheduling to RS)
	public Boolean installedToRs = false;
	
	public int sourceReg1;
	public int sourceReg2;
	
	//indicates whether or not the rob entry has something in it that "makes sense"
	//here, "makes sense" means that the simulator was responsible for allocating this instruction.
	//	otherwise, this is garbage or an instruction that has already been through the commit stage.
	public boolean inUse = false; 
	
	public boolean branchPredictedTaken;
	public boolean branchTaken;
	public int branchTarget;
	
	//pcPlusOne stores the value of the pc when this instruction was fetched, plus one
	public int pcPlusOne;
	
	//copy constructor
	public ReorderBufferEntry(ReorderBufferEntry sourceROBE){
		this.destRegisterId = sourceROBE.destRegisterId;
		this.destRegisterValue = sourceROBE.destRegisterValue;
		this.destRegisterValueValid = sourceROBE.destRegisterValueValid;
		this.instruction = sourceROBE.instruction;
		this.installedToRs = sourceROBE.installedToRs;
		
		this.branchPredictedTaken = sourceROBE.branchPredictedTaken;
		this.branchTaken = sourceROBE.branchTaken;
		
		this.pcPlusOne = sourceROBE.pcPlusOne;
		this.branchTarget = sourceROBE.branchTarget;
		
		this.inUse = sourceROBE.inUse;
		
		this.sourceReg1 = sourceROBE.sourceReg1;
		this.sourceReg2 = sourceROBE.sourceReg2;
	}
	
	//default constructor
	public ReorderBufferEntry(){
		this.destRegisterId = -1;
		this.destRegisterValue = -1;
		this.destRegisterValueValid = false;
		this.instruction = 0;
		this.installedToRs = false;
		
		this.branchPredictedTaken = false;
		this.branchTaken = false;
		
		this.pcPlusOne = 0;
		this.branchTarget = 0;
		
		this.inUse = false;
		
		this.sourceReg1 = -1;
		this.sourceReg2 = -1;
	}
	
}
